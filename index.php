﻿<?php
require_once('library/php/class/products.class.php');
$products = new products();

$allProducts = $products->getProducts();
$allProductsAsJson = $products->getAsJson();
?>
<!doctype html>
<html lang="sv">
<head>
    <meta charset="UTF-8">
    <title>Kaisa - Utmaning</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Monoton|Roboto:100,300" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="library/assets/css/style.css"/>
</head>

<body>
<header>
    <h1 class="text-center">dannes favoriter</h1>
</header>
<div class="container text-center">
    <div class="row">
        <?php foreach ($allProducts as $product) : ?>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 cover-images" style="background: url('<?= $product['coverImg'] ?>') no-repeat;background-position: center center; -webkit-background-size: 100% auto;-moz-background-size: 100% auto;-o-background-size: 100% auto;background-size: 90% auto;height: 450px;">
                    <div class="overlay"> </div>
                        <div class="show-movie">
                            <div class="col-xs-6 col-sm-12 col-md-6 col-lg-4 desc-button">
                                <button data-toggle="modal" data-target="#myModal<?= $product['id'] ?>">Beskrivning</button>
                            </div>
                        </div>
                </div>
               <div id="myModal<?= $product['id'] ?>" class="custom-modal modal fade" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body">
                            <h1><?= $product['name']?></h1>
                            <p><?= $product['desc']?></p>
                            <p><?= $product['price'] ?></p>
                            <div class="row">
	                        <?php foreach ($product['images'] as $value) { ?>
                                    <div class="col-sm-6 col-md-6 col-lg-3">
                                        <div class="thumbnail">
                                        <img src="<?php echo $value?>">
                                        </div>
                                    </div>
                                    <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
<footer>
    <div class="text-center">
        <p>Kaisa Svensson</p>
    </div>
</footer>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.bundle.min.js" integrity="sha384-feJI7QwhOS+hwpX2zkaeJQjeiwlhOP+SdQDqhgvvo1DsjtiSQByFdThsxO669S2D" crossorigin="anonymous"></script>
<script type="text/javascript" src="library/assets/js/script.js"></script>
<script type="text/javascript">
    console.table(<?=$allProductsAsJson?>);
</script>
</body>
</html>