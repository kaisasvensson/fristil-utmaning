<?php

/**
 * Class products
 *
 * En grundläggande php klass för att hantering av produkter.
 * Du får modifiera, extenda och bygga ut klassen precis som du vill
 * om du känner att det underlättar eller förbättrar slutresultatet.
 *
 */
class products {

	protected $productList;


	public function __construct() {


		$this->productList = array(
			array(
				'id' => '22',
				'name' => 'Interstellar',
				'desc' => 'Från regissören Christopher Nolan får vi berättelsen om ett gäng pionjärer som tar sig an det viktigaste uppdraget i mänsklighetens historia. Oscarsvinnaren Matthew McConaughey spelar Cooper som leder en expedition bortom vår egen galax för att ta reda på om vi människor har en framtid bland stjärnorna. Oscarsvinnaren Anne Hathaway and oscarsnominerade Jessica Chastain har också roller i filmen.',
				'price' => '199 kr',
				'coverImg' => 'https://lh3.googleusercontent.com/vXf6QbGLcvG1j7R5uD8Ly7Larl7ZeMF2RecZEsS5VOWkZa_fQNBOBb8DZ-52lp7tRgiR=w400-h600',
				'images' => array(
					'url' => 'http://pixel.nymag.com/imgs/daily/vulture/2014/08/05/05-interstellar-3.w529.h352.jpg',
					'url2' => 'https://img.purch.com/w/660/aHR0cDovL3d3dy5zcGFjZS5jb20vaW1hZ2VzL2kvMDAwLzA0My8xNDgvb3JpZ2luYWwvaW50ZXJzdGVsbGFyLXRyYWlsZXItZ3JhYi5qcGc=',
					'url3' => 'https://nerdist.com/wp-content/uploads/2015/03/interstellar-game-feature-03172015-970x545.jpg',
					'url4' => 'https://vignette.wikia.nocookie.net/interstellarfilm/images/f/f5/Interstellar-case-1024x578.jpg/revision/latest?cb=20171012074547'

				)
			),

			array(
				'id' => '65',
				'name' => 'Furry',
				'desc' => 'April 1945. När de allierade gör sitt slutliga drag på det europeiska spelbrädet ger sig den stridsmärkte officeren Wardaddy (Brad Pitt) ut på ett dödligt uppdrag bakom fiendens linjer med en Sherman-stridsvagn och ett team på fem man till sitt förfogande.',
				'price' => '179 kr',
				'coverImg' => 'http://www.freakingnews.com/Pictures/5/Misspelled-Movies-4872.jpg',
				'images' => array(
					'url' => 'http://www.wearemoviegeeks.com/wp-content/uploads/FURY-PK-10_DF-08601.jpg',
					'url2' => 'http://www.impulsegamer.com/articles/wp-content/uploads/2014/10/fury01-620x350.jpg',
					'url3' => 'https://timedotcom.files.wordpress.com/2014/10/fury-brad-pitt.jpg?quality=85',
					'url4' => 'https://images-na.ssl-images-amazon.com/images/M/MV5BMjAxODA4ODIxNl5BMl5BanBnXkFtZTgwNDc2MDcxMzE@._V1_.jpg'
				)
			),

			array(
				'id' => '523',
				'name' => 'Arrested Development',
				'desc' => 'I denna hyllade komediserie möter du Michael - den ende som är normal i den helt hopplösa familjen Bluth. Hans pappa George, familjeförsörjaren, har just hamnat i fängelse för sina skumma affärer - och han verkar trivas alldeles för bra därinne! Michael försöker nu förgäves städa upp efter George, och samtidigt förklara för sin galna familj att de inte kan leva ett helt obekymrat lyxliv längre...',
				'price' => '99 kr',
				'coverImg' => 'http://s.cdon.com/media-dynamic/images/product/000/441/441964.jpg',
				'images' => array(
					'url' => 'https://pmctvline2.files.wordpress.com/2017/01/ad.jpg?w=620&h=440&crop=1',
					'url2' => 'https://pmcdeadline2.files.wordpress.com/2015/06/arrested-development-2.jpg?w=446&h=299&crop=1',
					'url3' => 'http://static1.businessinsider.com/image/53c92741eab8ea1b68fc6da1-2400/arrested-development-season-4.jpg',
					'url4' => 'https://ewedit.files.wordpress.com/2017/03/arrested-development.jpg'
				)
			),

			array(
				'id' => '134',
				'name' => 'The Shining',
				'desc' => 'Stanley Kubrick utgår från ett manus som han medbearbetat från Stephen Kings roman och tillför scenbilder, kameraarbete och chock efter chock för att skapa något verkligt makabert. Jack Nicholson spelar Jack Torrance som besöker Overlook Hotel med sin fru (Shelley Duvall) och son (Danny Lloyd). Har Torrance aldrig varit där förut? Svaret kan hittas i en tidsförvrängning av galenskap och mord.',
				'price' => '129 kr',
				'coverImg' => 'http://14gjcfxf0002x9dzt48rde51.wpengine.netdna-cdn.com/wp-content/uploads/2017/09/10.05.17_TheShiningiuwyetriu-e1506021877995.jpg',
				'images' => array(
					'url' => 'https://www.maxim.com/.image/t_share/MTM4MDYxMDA5NjkxNjE3MTMy/shiningdannyjpg.jpg',
					'url2' => 'http://cdn-static.denofgeek.com/sites/denofgeek/files/styles/main_wide/public/the-shining_0.jpg?itok=n6cWumv4',
					'url3' => 'https://static1.squarespace.com/static/51b3dc8ee4b051b96ceb10de/t/5922fc27d482e90fb19febf8/1495465004972/?format=750w',
					'url4' => 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ-ogRhHFhFhpO0fJCbiCTruTL_3o87rfZTjRDw1rudUKPoTZFk'
				)
			),

			array(
				'id' => '136',
				'name' => 'Sagan om ringen',
				'desc' => 'A meek Hobbit from the Shire and eight companions set out on a journey to destroy the powerful One Ring and save Middle-earth from the Dark Lord Sauron.',
				'price' => '299 kr',
				'coverImg' => 'http://is5.mzstatic.com/image/thumb/Video1/v4/df/9e/dd/df9edd56-91a4-422e-1b26-edb03bc96750/source/1200x630bb.jpg',
				'images' => array(
					'url' => 'https://cdn.amctheatres.com/general/afi-img/9.6_fantasy_hero_solo_lotr.jpg',
					'url2' => 'https://consequenceofsound.files.wordpress.com/2017/11/lord-of-the-rings.jpg?quality=80&w=807',
					'url3' => 'http://s3.amazonaws.com/digitaltrends-uploads-prod/2017/11/amazon-lotr-casting-header.jpg',
					'url4' => 'https://cdn1.thr.com/sites/default/files/2016/11/lord_rings_2001_51-h_2016.jpg'
				)
			),

			array(
				'id' => '42',
				'name' => 'Fargo',
				'desc' => 'Lorne Malvo är en manipulativ brottsling som förändrar försäkringsförsäljaren Lesters liv. Två småstadspoliser slår sig samman och gör allt för att stoppa den destruktive Malvo',
				'price' => '99 kr',
				'coverImg' => 'http://s.cdon.com/media-dynamic/images/product/movie/dvd/image4/fargo_-_season_1_3_disc_nordic-26947089-frntl.jpg',
				'images' => array(
					'url' => 'http://publicradio1.wpengine.netdna-cdn.com/statewide/files/2014/04/MARTIN_FREEMAN_02_PARKING_LOT_067.jpg',
					'url2' => 'http://img.thedailybeast.com/image/upload/v1492200401/articles/2014/04/10/is-tv-s-fargo-as-bloody-good-as-the-coen-brothers-movie-you-betcha/140410-fallon-fargo2-tease_evhmny.jpg',
					'url3' => 'https://nerdist.com/wp-content/uploads/2015/07/516875.jpg',
					'url4' => 'https://i.ytimg.com/vi/gKs8DzjPDMU/maxresdefault.jpg'
				)
			)
		);


	}


	/**
	 * getProducts
	 *
	 * Funktion för att hämta alla produkter.
	 *
	 * @return array
	 */
	public function getProducts() {
		return $this->productList;
	}


	/**
	 * getAsJson
	 *
	 * Funktion för att hämta ut alla produkter som JSON ifall du föredrar det
	 *
	 * @return string
	 */
	public function getAsJson() {
		return json_encode($this->productList);
	}

}